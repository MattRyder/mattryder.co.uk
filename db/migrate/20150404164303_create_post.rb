class CreatePost < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :title
      t.string :article
      t.string :category
      t.string :slug
      t.timestamps
    end
  end
end
