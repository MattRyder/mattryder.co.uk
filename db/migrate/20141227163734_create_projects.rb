class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.string :title
      t.text :novice_description
      t.text :intermediate_description
      t.text :advanced_description
      t.string :link

      t.timestamps
    end
  end
end
