class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :set_technical_level

  def set_technical_level
    cookies[:technical_level] = "Intermediate" if cookies[:technical_level].blank?
  end
end
