class PostsController < ApplicationController
  before_action :set_post, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_admin!, except: [:index, :show]

  def index
    @posts = Post.all
    @page_title       = 'Posts'
    @page_description = 'Short writings and notes about software development from Matt Ryder'
    @page_keywords    = 'Matt Ryder, Software, Rails, Ruby, Java, gamedev, Development'
  end

  def show
    @page_title       = @post.title
    @page_description = @post.summary
    counts = @post.article.split(" ").each_with_object(Hash.new(0)) { |e, h| h[e] += 1 }
    counts = counts.sort_by{|k,v| v}.reverse.first(10).map{|k,v| k} if counts.present?

    @page_keywords    = 'Matt Ryder, Software, Rails, Ruby, ' + counts.join(", ")
  end

  def new
    @post = Post.new
  end

  def edit
  end

  def create
    @post = Post.new(project_params)

    respond_to do |format|
      if @post.save
        format.html { redirect_to @post, notice: 'Project was successfully created.' }
        format.json { render :show, status: :created, location: @post }
      else
        format.html { render :new }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @post.update(project_params)
        format.html { redirect_to @post, notice: 'Project was successfully updated.' }
        format.json { render :show, status: :ok, location: @post }
      else
        format.html { render :edit }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @post.destroy
    respond_to do |format|
      format.html { redirect_to projects_url, notice: 'Project was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.friendly.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def project_params
      params.require(:post).permit(:title, :article, :summary, :category)
    end
end
